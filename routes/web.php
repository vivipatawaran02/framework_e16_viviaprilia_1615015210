<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tes', function () {
    return view('master');
});

Route::get('/pengguna','PenggunaController@awal');
Route::get('/save','PenggunaController@simpan');

Route::get('/buku','BukuControllerk@index');
Route::get('/tambah','BukuControllerk@show');

Route::get('/tambah/penulis','PenulisController@create');
Route::post('/penulis/simpan','PenulisController@store');
Route::get('/penulis','PenulisController@index');
Route::get('/penulis/edit/{penulis}','PenulisController@edit');
Route::post('/penulis/update/{penulis}','PenulisController@update');
Route::get('/penulis/hapus/{penulis}','PenulisController@destroy');

Route::get('/tambah/buku','BukuController@create');
Route::post('/buku/simpan','BukuController@store');

Route::get('/kategori/tambah','KategoriController@create');
Route::post('/kategori/simpan','KategoriController@store');
Route::get('/kategori/edit/{simpan}','KategoriController@edit');
Route::post('/kategori/update/{simpan}','KategoriController@update');
Route::get('/kategori/hapus/{simpan}','KategoriController@destroy');
Route::get('/kategori','KategoriController@index');

Route::get('/Pembeli/tambah','PembeliController@create');
Route::post('/Pembeli/simpan','PembeliController@store');
Route::get('/Pembeli','PembeliController@index');
Route::get('/Pembeli/edit/{pembeli}','PembeliController@edit');
Route::post('/Pembeli/update/{pembeli}','PembeliController@update');
Route::get('/Pembeli/hapus/{pembeli}','PembeliController@destroy');

Route::get('/admin','AdminController@index');
Route::get('/admin/tambah','AdminController@create');
Route::post('/admin/simpan','AdminController@store');
Route::get('/admin/lihat','AdminController@show');
Route::get('/admin/edit/{admin}','AdminController@edit');
Route::post('/admin/update/{admin}','AdminController@update');
Route::get('/admin/hapus/{admin}','AdminController@destroy');



